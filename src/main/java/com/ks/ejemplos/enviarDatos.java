package com.ks.ejemplos;

/**
 * Created by Miguel on 19/09/2016.
 */
public class enviarDatos
{
    private EventosTCP eventosTCP;

    public void setEventosTCP(EventosTCP eventosTCP)
    {
        this.eventosTCP = eventosTCP;
    }

    public void llamadaMetodos()
    {
        eventosTCP.conexionEstablecida();
        eventosTCP.datosRecibidos("prueba");
    }
}
