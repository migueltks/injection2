package com.ks.ejemplos;

/**
 * Created by Miguel on 19/09/2016.
 */
public interface EventosTCP
{
    public void conexionEstablecida();

    public void datosRecibidos(String dato);
}
